<?php
/*
Plugin Name: ELI Course Embed
Plugin URI: http://wooninjas.com
Description: This plugin gets ELI courses and displays them via short-code.
Version: 1.0.0
Author: Wooninjas
Author URI: http://wooninjas.com
License: GPLv2
*/

class ELI_Course_Embed
{
    public static $instance = null;

    /**
     * Creates or returns an instance of this class.
     * @since    1.0.0
     * @return ELI_Course_Embed A single instance of this class.
     */
    public static function get()
    {
        if (self::$instance === null)
            self::$instance = new self();

        return self::$instance;
    }

    /**
     *
     * @since    1.0.0
     */
    function __construct()
    {
        self::hooks();
    }

    /**
     * Plugin hooks
     * @since    1.0.0
     */
    public function hooks()
    {
        add_action('wp_enqueue_scripts', array($this, 'add_plugin_scripts'));
        add_shortcode('eli_course_embed', array($this, 'get_request'));

        include(plugin_dir_path(__FILE__) . '/includes/class-course-embed-settings.php');
    }

    /**
     * Adding plugin scripts
     * @since    1.0.0
     */
    function add_plugin_scripts()
    {
        $page_slug = trim($_SERVER["REQUEST_URI"], '/');
        $options = get_option('course_embed_settings');

        if(!empty($options)) {
            $page_id = $options["eli_api_page"];
        }

        if(!empty($page_id)) {
            $page_name = get_post_field('post_name', $page_id);
        }

        if ($page_slug == $page_name) {
            wp_enqueue_style('sm-style', plugin_dir_url(__FILE__) . 'assets/css/smart-meetings.css', false, time());
        }

        wp_enqueue_style('custom-fa', 'https://use.fontawesome.com/releases/v5.0.6/css/all.css');
        wp_enqueue_style('plugin-style', plugin_dir_url(__FILE__) . 'assets/css/eli-embed-style.css', false, time(), 'all');
        wp_enqueue_style('owlcarousel-style', plugin_dir_url(__FILE__) . 'assets/owl/owl.carousel.min.css', '', time());
        wp_enqueue_style('css_course_embed', plugin_dir_url(__FILE__) . 'assets/css/course_embed.css', false, time());
        wp_enqueue_script('owl-carousel', plugin_dir_url(__FILE__) . 'assets/owl/owl.carousel.min.js', array('jquery'), time());
        wp_register_script('js_course_embed', plugin_dir_url(__FILE__) . 'assets/js/course_embed.js', array('jquery'), time(), true);
        wp_enqueue_script('js_course_embed', plugin_dir_url(__FILE__) . 'assets/js/course_embed.js', array('jquery'), time(), true);
    }

    /**
     * Get courses by sending a request by course ids.
     * @since    1.0.0
     */
    function get_request()
    {
        ob_start();
        $options = get_option('course_embed_settings');
        $api_key = $options["api_key"];

        $api_data = get_transient("api_eli_data");
        if(!empty($api_data)){
            echo $this->api_business_logic($api_data);
        }
        else {
            $url = "https://www.eventleadershipinstitute.com/wp-json/eli_course_api/v1/course/key=" . $api_key;
            $args = array(
                'timeout' => 100,
                'sslverify' => false
            );

            $response = wp_remote_get($url, $args);
            //$response = "";
            $response_body = wp_remote_retrieve_body($response);
            $eli_courses = json_decode($response_body, true);

            if (!empty($eli_courses["msg"])) {
                echo "API key is not correct, please re-check your API key in Course Embed Settings.";
                return ob_get_clean();
            } else {
                if (!empty($eli_courses)) {
                    echo $this->api_business_logic($eli_courses,1);
                }
                else {
                    echo "API key is correct but there is some issue with fetching data, either API hosting server is down or some other error. Please re-check in few minutes";
                    return ob_get_clean();
                }
            }
        }
        return ob_get_clean();
    }

    function api_business_logic($api_data,$set_transient=0)
    {
        $cats = [];
        $data = [];

        foreach ($api_data as $courses) {
            if(!empty($courses["cat_name"]) || $courses["cat_name"] !== NULL) {
                if (!in_array($courses["cat_name"], $cats)) {
                    array_push($cats, $courses["cat_name"]);
                }
            }
        }

        if(!empty($cats)) {
            foreach ($cats as $cat) {
                if (!empty($cat)) {
                    foreach ($api_data as $eli_course) {
                        if (!empty($eli_course["cat_name"]) || $courses["cat_name"] !== NULL) {
                            if ($eli_course["cat_name"] == $cat) {
                                $data[$cat][] = array(
                                    "title" => $eli_course["title"],
                                    "image" => $eli_course["image"],
                                    "cmp" => $eli_course["cmp"],
                                    "length" => $eli_course["length"],
                                    "id" => $eli_course["id"],
                                    "instructor" => $eli_course["instructor"],
                                    "guid" => $eli_course["guid"],
                                    "api_slug" => $eli_course["api_slug"],
                                    "affiliate" => $eli_course["affiliate"]
                                );
                            }
                        }
                    }
                }
            }
        }

        foreach ($api_data as $eli_course) {
            if (isset($eli_course["theme_color"]) || isset($eli_course["searchbar_color"]) || isset($eli_course["category_icons_color"])
                || isset($eli_course["courses_title_color"]) || isset($eli_course["headings_color"])
                || isset($eli_course["pagination_dots_color"])) {
                $data["colors"] = array(
                    "theme_color" => $eli_course["theme_color"],
                    "searchbar_color" => $eli_course["searchbar_color"],
                    "category_icons_color" => $eli_course["category_icons_color"],
                    "courses_title_color" => $eli_course["courses_title_color"],
                    "headings_color" => $eli_course["headings_color"],
                    "pagination_dots_color" => $eli_course["pagination_dots_color"],
                );
                wp_localize_script('js_course_embed', 'js_object', array('right_nav' => $eli_course["right_nav"], 'left_nav' => $eli_course["left_nav"]));
            }
        }

        $this->display_header($api_data[0]["api_slug"], $api_data[0]["affiliate"], $cats);
        $this->display_courses($data);

        if($set_transient == 1) {
            set_transient("api_eli_data", $api_data, (60 * 60 * 6));
        }
    }

    /**
     * Get all categories
     * @since    1.0.0
     */
    function display_header($slug, $affiliate='', $categories)
    {
        $cats = [];
        foreach ($categories as $category) {
            if (!empty($category)) {
                $lowercase_cats = strtolower($category);
                array_push($cats, str_replace("&amp;","&",$lowercase_cats));
            }
        }

        ?>
        <style>
            .entry-page .entry-content > * {
                max-width: 90rem;
            }

            .site-navigation li {
                flex-wrap: nowrap;
            }
        </style>

        <div class="search_section course">
            <div class="course_search">
                <div class="search_column_1">
                    <img src="<?php echo plugin_dir_url(__FILE__) . '/assets/images/smart-meetings.png'; ?>" />
                </div>
                <div class="search_column_2">
                    <form role="search" method="get" class="et_pb_searchform" action="https://www.eventleadershipinstitute.com/">
                        <label class="screen-reader-text" for="s">Search for:</label>
                        <input type="text" name="s" placeholder="Search for a course..." class="et_pb_s">
                        <input type="hidden" name="et_pb_searchform_submit" value="et_search_proccess">
                        <input type="hidden" name="referrer" value="<?php echo $slug ?>">
                        <input type="submit" value="Search" class="et_pb_searchsubmit" style="">
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <?php
        if(!empty($affiliate) && !empty($slug)){
            $link = "?referrer=".$slug."&".$affiliate;
        }
        else if(empty($affiliate) && !empty($slug)){
            $link = "?referrer=".$slug;
        }
        else{
            $link = "";
        }

        $static_cats = ["business & sales","careers","creativity & design","event leaders q&a","maverick series","planning & logistics","strategy & roi","technical production","venues & destinations","venues & destinations","white papers & e-books","industry presentations","cmp credit"];
        $cat_count = 0;
        
        foreach($cats as $key => $cat){
            if(in_array($cat,$static_cats)) {
                $count = $cat_count+1;
                if ($cat == "business & sales") {
                    $title = "Business & Sales";
                    $link = "business-sales";
                    $icon = '<img src="' . plugin_dir_url(__FILE__) . 'assets/images/Icons-03.png' . '"/>';
                } else if ($cat == "careers") {
                    $title = "Careers";
                    $link = "careers";
                    $icon = '<i class="fas fa-user"></i>';
                } else if ($cat == "creativity & design") {
                    $title = "Creativity & Design";
                    $link = "creativity-design";
                    $icon = '<i class="fas fa-lightbulb"></i>';
                } else if ($cat == "event leaders q&a") {
                    $title = "Event Leaders Q&A";
                    $link = "event-leaders-qa";
                    $icon = '<i class="fas fa-comment"></i>';
                } else if ($cat == "maverick series") {
                    $title = "Maverick Series";
                    $link = "maverick-series";
                    $icon = '<i class="fas fa-star"></i>';
                } else if ($cat == "planning & logistics") {
                    $title = "Planning & Logistics";
                    $link = "planning-logistics";
                    $icon = '<i class="fas fa-file-alt"></i>';
                } else if ($cat == "strategy & roi") {
                    $title = "Strategy & ROI";
                    $link = "strategy-roi";
                    $icon = '<i class="fas fa-compass"></i>';
                } else if ($cat == "technical production") {
                    $title = "Technical Production";
                    $link = "technical-production";
                    $icon = '<i class="fas fa-microphone"></i>';
                } else if ($cat == "venues & destinations") {
                    $title = "Venues & Destinations";
                    $link = "venues-destinations";
                    $icon = '<i class="fas fa-map-marker-alt"></i>';
                } else if ($cat == "white papers & e-books") {
                    $title = "White Papers & E-Books";
                    $link = "white-papers-ebooks";
                    $icon = '<i class="fas fa-book"></i>';
                } else if ($cat == "industry presentations") {
                    $title = "Industry Presentations";
                    $link = "industry-presentations";
                    $icon = '<img src="' . plugin_dir_url(__FILE__) . '/assets/images/Icons-01.png' . '"/>';
                } else if ($cat == "cmp credit") {
                    $title = "CMP Credit";
                    $link = "cmp-credit";
                    $icon = '<img src="' . plugin_dir_url(__FILE__) . '/assets/images/Icons-02.png' . '"/>';
                } else {
                    $icon = "";
                }

                if($count % 5 == 1){
                    echo '<div class="eli_row">';
                }
                ?>
                <div class="eli_column">
                    <a href="https://www.eventleadershipinstitute.com/course-category/<?php echo $link;?>/<?php echo $link ?>">
                        <div class="eli_content">
                            <div class="eli_image">
                                <?php echo $icon; ?>
                            </div>
                            <div class="eli_cat-name"><?php echo $title; ?></div>
                        </div>
                    </a>
                </div>
                <?php
                if(($count % 5 == 0)){
                    echo '</div>';
                }


                if(($key+1) > count($cats) || ($key+1) == count($cats)){
                    echo '</div>';
                }
                $cat_count++;
            }
        }
    }

    function display_courses($eli_courses)
    {
        if (!empty($eli_courses)):
            $theme_color = '';
            $searchbar_color = '';
            $category_icons_color = '';
            $courses_title_color = '';
            $headings_color = '';
            $pagination_dots_color = '';
            ?>
            <div class="search-category course-embed">
                <div id="content-area" class="clearfix">
                    <ul class="llms-loop-list llms-course-list cols-5">
                        <?php foreach ($eli_courses as $key => $eli_courses_data): ?>
                            <?php if ($key != "colors"): ?>
                                <h2><?php echo $key; ?></h2>
                                <div class="owl-carousel">
                                    <?php
                                    if (!empty($eli_courses_data)):
                                        foreach ($eli_courses_data as $eli_course):
                                            $course_details = array();
                                            $course_details['title'] = isset($eli_course['title']) ? $eli_course['title'] : '';
                                            $course_details['image'] = isset($eli_course['image']) ? $eli_course['image'] : '';
                                            $course_details['cmp'] = isset($eli_course['cmp']) ? $eli_course['cmp'] : '';
                                            $course_details['length'] = isset($eli_course['length']) ? $eli_course['length'] : '';
                                            $course_details['id'] = isset($eli_course['id']) ? $eli_course['id'] : '';
                                            $course_details['instructor'] = isset($eli_course['instructor']) ? $eli_course['instructor'] : '';
                                            $course_details['guid'] = isset($eli_course['guid']) ? $eli_course['guid'] : '';
                                            $course_details['api_slug'] = isset($eli_course['api_slug']) ? $eli_course['api_slug'] : '';
                                            $course_details['affiliate'] = isset($eli_course['affiliate']) ? $eli_course['affiliate'] : '';
                                            ?>
                                            <li class="llms-loop-item post-<?php $course_details['id']; ?> course type-course status-publish has-post-thumbnail hentry item">
                                                <a href="<?php echo $course_details['guid'] . '?referrer=' . $course_details['api_slug']; ?>&<?php echo $eli_course['affiliate'] ?>">
                                                    <div class="llms-loop-item-content">
                                                        <div class="llms-loop-item-image"><?php echo $course_details['image']; ?></div>
                                                        <h4 class="llms-loop-title"><?php echo $course_details['title']; ?></h4>
                                                        <div class="course-instructor">
                                                            <img src="<?php echo plugin_dir_url(__FILE__) . '/assets/images/Instructor.png' ?>"/>
                                                            <span><?php echo $course_details['instructor']; ?></span>
                                                        </div>
                                                        <div class="time-ce">
                                                            <div class="time-length">
                                                                <img src="<?php echo plugin_dir_url(__FILE__) . '/assets/images/course-length.png' ?>"/>
                                                                <span>
                                                    <?php echo $course_details['length']; ?>
                                                </span>
                                                            </div>
                                                            <div class="course-ce">
                                                                <img src="<?php echo plugin_dir_url(__FILE__) . '/assets/images/cmp.png' ?>"/>
                                                                <span>
                                                        <?php echo $course_details['cmp']; ?>
                                                    </span>
                                                            </div>
                                                        </div> <!-- .time-ce -->
                                                        <div class="llms-meta llms-cmp-hours">
                                                            <p></p>
                                                        </div>
                                                    </div><!-- .llms-loop-item-content -->
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            <?php else:
                                $theme_color = $eli_courses_data["theme_color"];
                                $searchbar_color = $eli_courses_data["searchbar_color"];
                                $category_icons_color = $eli_courses_data["category_icons_color"];
                                $courses_title_color = $eli_courses_data["courses_title_color"];
                                $headings_color = $eli_courses_data["headings_color"];
                                $pagination_dots_color = $eli_courses_data["pagination_dots_color"];

                                echo "<style>
                                    .llms-loop-item-content{
                                        background: " . $theme_color . " !important;
                                    }
                                    ul.llms-loop-list h2{
                                        color: " . $headings_color . " !important;
                                    }
                                    .eli_content .eli_image {
                                        background-color: " . $category_icons_color . " !important;
                                    }
                                    div.owl-carousel .owl-dots .owl-dot.active span, div.owl-carousel .owl-dots .owl-dot:hover span{
                                        background: " . $pagination_dots_color . " !important;
                                    }
                                </style>";
                            endif ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <?php endif;
    }
}

if (is_admin()) {
    ELI_Course_Embed::get();
}

$page_slug = trim($_SERVER["REQUEST_URI"], '/');
$options = get_option('course_embed_settings');

if(!empty($options)) {
    $page_id = $options["eli_api_page"];
}

if(!empty($page_id)) {
    $page_name = get_post_field('post_name', $page_id);
}

if ($page_slug == $page_name) {
    ELI_Course_Embed::get();
}

function clear_eli_api_transient()
{
    $msg = "";
    $transient = get_transient("api_eli_data");
    if($transient) {
        delete_transient("api_eli_data");
        $msg = "Transient deleted";
    }
    else{
        $msg = "Transient not found";
    }
    return $msg;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'flush_eliapi_cache/v1', '/flush/time=(?P<key>[a-zA-Z0-9-]+)', array(
        'methods' => 'GET',
        'callback' => 'clear_eli_api_transient',
    ) );
},999 );
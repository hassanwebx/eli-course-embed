<?php

class Course_Embed_Settings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Course Embed Settings',
            'Course Embed Settings',
            'manage_options',
            'course_embed_settings',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'course_embed_settings' );
        ?>
        <div class="wrap">
            <h1>Course Embed Settings</h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields( 'course_embed_group' );
                do_settings_sections( 'course_embed_settings' );
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'course_embed_group', // Option group
            'course_embed_settings', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'API Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'course_embed_settings' // Page
        );

        add_settings_field(
            'api_key', // ID
            'API Key', // Title
            array( $this, 'api_key_callback' ), // Callback
            'course_embed_settings', // Page
            'setting_section_id' // Section           
        );

        add_settings_section(
            'setting_section_id', // ID
            'Shortcode', // Title
            array( $this, 'print_section_info' ), // Callback
            'course_embed_settings' // Page
        );

        add_settings_field(
            'shortcode', // ID
            'Shortcode', // Title
            array( $this, 'shortcode_callback' ), // Callback
            'course_embed_settings', // Page
            'setting_section_id' // Section
        );

        add_settings_section(
            'setting_section_id', // ID
            'API Embed Page', // Title
            array( $this, 'print_section_info' ), // Callback
            'course_embed_settings' // Page
        );

        add_settings_field(
            'api_embed_page', // ID
            'API Embed Page', // Title
            array( $this, 'select_shortcode_page_callback' ), // Callback
            'course_embed_settings', // Page
            'setting_section_id' // Section
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['api_key'] ) )
            $new_input['api_key'] = $input['api_key'];

        if( isset( $input['eli_api_page'] ) )
            $new_input['eli_api_page'] = $input['eli_api_page'];

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your API key:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_key_callback()
    {
        printf(
            '<input type="text" id="api_key" name="course_embed_settings[api_key]" value="%s" />',
            isset( $this->options['api_key'] ) ? esc_attr( $this->options['api_key']) : ''
        );
    }

    public function shortcode_callback()
    {
        printf(
            '<input type="text" id="shortcode" name="shortcode" value="[eli_course_embed]" disabled="true" style="color:black;">'
        );
    }

    public function select_shortcode_page_callback()
    {
        $args = array(
            'name'  => 'course_embed_settings[eli_api_page]',
            'show_option_none' => 'Select API embed page',
            'selected' => isset( $this->options['eli_api_page'] ) ? $this->options['eli_api_page'] : ''
        );

        wp_dropdown_pages($args);
    }
}

if(is_admin()) {
    $my_settings_page = new Course_Embed_Settings();
}
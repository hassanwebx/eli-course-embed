<?php

/**
 * Template Name: ELI Courses Template
 */

get_header();
?>
<div class="et_pb_section et_pb_section_1 et_section_regular">
				
				
				
				
					<div class="et_pb_row et_pb_row_1 et_pb_row_6col">
				<div class="et_pb_column et_pb_column_1_6 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_0 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/business-sales/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/business-sales/">Business &amp; Sales</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_1 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/careers/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/careers/">Careers</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_2 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/creativity-design/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/creativity-design/">Creativity &amp; Design</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_4  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_3 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/event-leaders-qa/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/event-leaders-qa/">Event Leaders Q&amp;A</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_5  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_4 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/maverick-series/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/maverick-series/">Maverick Series</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_5 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/planning-logistics/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/planning-logistics/">Planning &amp; Logistics</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_row et_pb_row_2 et_pb_row_6col">
				<div class="et_pb_column et_pb_column_1_6 et_pb_column_7  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_6 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/strategy-roi/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/strategy-roi/">Strategy &amp; ROI</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_8  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_7 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/technical-production/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/technical-production/">Technical Production</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_8 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/venues-destinations/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/venues-destinations/">Venues &amp; Destinations</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_10  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_9 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/white-papers-ebooks/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/white-papers-ebooks/">White Papers &amp; E-Books</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_11  et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_10 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/industry-presentations/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/industry-presentations/">Industry Presentations</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_6 et_pb_column_12  et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_11 et_clickable  et_pb_text_align_left  et_pb_blurb_position_left et_pb_bg_layout_light">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><a href="/course-category/cmp-credit/"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-pb-icon-circle et-animated"></span></span></a></div>
					<div class="et_pb_blurb_container">
						<h1 class="et_pb_module_header"><a href="/course-category/cmp-credit/">CMP Credit</a></h1>
						
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div>

 

/**
 * Owl Carousel v2.3.4
 * Initializing the plugin
 */
jQuery(function(jQuery) {
  if( jQuery( '.owl-carousel').length ) {

    jQuery( ".owl-carousel" ).owlCarousel({
      nav:true, // Show next and prev buttons
      navText: [ "<img src='" + site_url + "/left-nav-purple.png' style=\"height:70px\">","<img src='" + site_url + "/right-nav-purple.png' style=\"height:70px\">"],
      dots: true,
	    loop:true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      responsiveClass:true,
      responsive:{
          0:{
              dotsEach: 8,
              items:1,
              nav:true
          },
          700:{
              dotsEach: 5,
              items:3,
              nav:true
          },
          1000:{
              items:5,
              nav:true,
              loop:true
          }
      }
    });

  }
});
jQuery(document).ready(function(){
    jQuery( ".owl-carousel" ).owlCarousel({
        nav:true, // Show next and prev buttons
        navText: [ "<img src='" + js_object.left_nav + "' style=\"height:70px\">","<img src='" + js_object.right_nav + "' style=\"height:70px\">"],
        dots: true,
        loop:true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        responsiveClass:true,
        responsive:{
            0:{
                dotsEach: 8,
                items:1,
                nav:true
            },
            700:{
                dotsEach: 5,
                items:3,
                nav:true
            },
            1000:{
                items:5,
                nav:true,
                loop:true
            }
        }
    });
});